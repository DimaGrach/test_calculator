﻿using UnityEngine;
using System.Data;

public class Operations : CalculateManager
{
    private string resul;

    public void Result()
    {
        DataTable dt = new DataTable();
        //Замена всех запятых на точки, так как при вычислении C# в десятичных цыфрах понимает только точки.
        string newString = resulField.text.Replace(",", ".");
        //Проводим армефметические операции
        try 
        { 
            resul = dt.Compute(newString, "").ToString(); 
        }
        catch
        {
            try 
            {
                newString = newString.Remove(newString.Length - 1);
                resul = dt.Compute(newString, "").ToString();
            }
            catch
            {
                resul = "Error!";
            }
        }
       
        
        resulField.text = resul;
    }

    public void ResetAll()
    {
        resulField.text = "";
    }

    public void DeleteNumber()
    {
        if (resulField.text.Length > 0)
            resulField.text = resulField.text.Remove(resulField.text.Length - 1);
    }

    public void Square()
    {

        try
        {
            float amount = float.Parse(resulField.text);
            resulField.text = Mathf.Sqrt(amount).ToString();
        }
        catch
        {
            resulField.text = "Error!";
        }
    }
}
