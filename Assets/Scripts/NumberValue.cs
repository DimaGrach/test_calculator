﻿using UnityEngine;

public class NumberValue : CalculateManager
{
    [SerializeField] string number;
    public void SetNumber()
    {
        if (resulField.text == "Infinity" || resulField.text == "-Infinity" || resulField.text == "Error!" || resulField.text == "NaN")
            resulField.text = number;
        else
            resulField.text += number;
    }

}
